import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/_helpers/auth.guard';
import { AppComponent } from './app.component';



const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'user', loadChildren: './user/user.module#UserModule', canActivate: [AuthGuard] },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
