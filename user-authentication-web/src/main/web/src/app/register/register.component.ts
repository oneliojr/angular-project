import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service'
import { User } from '../models/user';
import { FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers: [AppService],
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private _service: AppService) { }

  user: User = {} as User;

  ngOnInit(): void {

    this.registerForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, Validators.required),
      lastName: new FormControl(this.user.lastName, Validators.required),
      email: new FormControl(this.user.email, [Validators.required, Validators.email]),
      birthDate: new FormControl(this.user.birthDate, Validators.required),
      idCode: new FormControl(this.user.idCode, Validators.required),
      password: new FormControl(this.user.password, Validators.required),
    });
  }

  isFieldValid(field: AbstractControl) {
    return field.invalid && field.touched;
  }

  register() {

    console.log(this.registerForm);
    console.log(this.user);

    // this._service.postUser(form.value).subscribe((user: User) => {
    //   this.user = user;
    // });
  }

}
